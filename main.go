package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

type evccdata struct {
	Result struct {
		PVPower      float64 `json:"pvPower"`
		BatteryPower float64 `json:"batteryPower"`
		BatterySoC   float64 `json:"batterySoC"`
		HomePower    float64 `json:"homePower"`
		Loadpoints   []struct {
			Title       string  `json:"title"`
			ChargePower float64 `json:"chargePower"`
		} `json:"loadpoints"`
		Grid struct {
			Power float64 `json:"power"`
		}
	} `json:"result"`
}

var (
	address  = flag.String("listen", "localhost:8000", "the listen address for the metrics service")
	target   = flag.String("target", "http://192.168.0.1/api", "the target URL to query")
	mime     = flag.String("mime", "application/json", "the content type of the respose")
	user     = flag.String("user", "", "the basic auth user")
	password = flag.String("password", "", "the basic auth password")

	logger *zap.Logger

	pvGauge      prometheus.Gauge
	gridGauge    prometheus.Gauge
	batteryGauge prometheus.Gauge
	homeGauge    prometheus.Gauge
	socGauge     prometheus.Gauge

	loadpointGauges map[string]prometheus.Gauge
)

func metrics(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var cl http.Client
		rq, err := http.NewRequest(http.MethodGet, *target, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		rq.Header.Add("Accept", *mime)
		if *user != "" {
			rq.SetBasicAuth(*user, *password)
		}
		rsp, err := cl.Do(rq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer rsp.Body.Close()
		var res evccdata
		err = json.NewDecoder(rsp.Body).Decode(&res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if res.Result.BatterySoC < 0 {
			res.Result.BatterySoC = 0
		}
		registerLoadpoints(res)
		logger.Info("fetched data",
			zap.Float64("pvpower", res.Result.PVPower),
			zap.Float64("gridpower", res.Result.Grid.Power),
			zap.Float64("homepower", res.Result.HomePower),
			zap.Float64("batterypower", res.Result.BatteryPower),
			zap.Float64("batterysoc", res.Result.BatterySoC),
			zap.Any("loadpoints", res.Result.Loadpoints))
		pvGauge.Set(res.Result.PVPower)
		gridGauge.Set(res.Result.Grid.Power)
		homeGauge.Set(res.Result.HomePower)
		batteryGauge.Set(res.Result.BatteryPower)
		socGauge.Set(res.Result.BatterySoC)
		for _, lp := range res.Result.Loadpoints {
			g := loadpointGauges[lp.Title]
			if g != nil {
				g.Set(lp.ChargePower)
			}
		}

		h.ServeHTTP(w, r)
	})
}

func registerLoadpoints(evd evccdata) {
	if loadpointGauges == nil {
		loadpointGauges = make(map[string]prometheus.Gauge)
	}
	for _, lp := range evd.Result.Loadpoints {
		if _, has := loadpointGauges[lp.Title]; !has {
			g := prometheus.NewGauge(prometheus.GaugeOpts{
				Namespace: "lp",
				Subsystem: lp.Title,
				Name:      "current_power",
				Help:      "the current power charging power of this loadpoint",
			})
			_ = prometheus.Register(g)
			loadpointGauges[lp.Title] = g
		}
	}
}

func main() {
	l, err := zap.NewDevelopment()
	if err != nil {
		log.Fatalf("cannot initialize zap: %v", err)
	}
	logger = l

	flag.Parse()

	pvGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "site",
		Subsystem: "pv",
		Name:      "current_power",
		Help:      "the current power of the pv",
	})
	prometheus.MustRegister(pvGauge)
	gridGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "site",
		Subsystem: "grid",
		Name:      "current_power",
		Help:      "the current power consumption from/to grid",
	})
	prometheus.MustRegister(gridGauge)
	batteryGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "site",
		Subsystem: "battery",
		Name:      "current_power",
		Help:      "the current power consumption from/to battery",
	})
	prometheus.MustRegister(batteryGauge)
	homeGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "site",
		Subsystem: "home",
		Name:      "current_power",
		Help:      "the current power consumption from home",
	})
	prometheus.MustRegister(homeGauge)
	socGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "site",
		Subsystem: "batterysoc",
		Name:      "value",
		Help:      "the current soc of the battery",
	})
	prometheus.MustRegister(socGauge)

	http.Handle("/metrics", metrics(promhttp.Handler()))

	logger.Info("start server", zap.String("address", *address), zap.String("target", *target), zap.String("user", *user))
	_ = http.ListenAndServe(*address, nil)
}
