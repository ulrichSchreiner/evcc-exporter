build:
	mkdir -p bin
	go build -o bin/evcc-exporter

localrun: build
	bin/evcc-exporter -target http://192.168.0.22:7070/api/state